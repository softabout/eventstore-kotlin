package de.softabout


import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.connection.ClusterSettings
import com.mongodb.connection.ConnectionPoolSettings
import com.mongodb.connection.ServerSettings
import com.mongodb.connection.SocketSettings
import com.mongodb.connection.netty.NettyStreamFactoryFactory
import com.mongodb.reactivestreams.client.MongoClient
import com.mongodb.reactivestreams.client.MongoClients
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.mongo.MongoProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import javax.annotation.PreDestroy

private const val MAX_WAIT_QUEUE_SIZE = 5000

@Configuration
@ConditionalOnClass(MongoClient::class)
@EnableConfigurationProperties(MongoProperties::class)
@Profile("!test")
open class MongoClientConfig(private val properties: MongoProperties,
                             private val environment: Environment) {

    private lateinit var mongo: MongoClient

    @PreDestroy
    fun close() {
        this.mongo.close()
    }

    @Bean
    @ConditionalOnMissingBean
    open fun reactiveStreamsMongoClient(): MongoClient? {
        val connectionString = ConnectionString(
                this.properties.determineUri())
        val builder = MongoClientSettings.builder()
                .applyToClusterSettings { clusterSettingsBuilder ->
                    clusterSettingsBuilder.applySettings(ClusterSettings.builder()
                            .applyConnectionString(connectionString).build())
                }
                .applyToConnectionPoolSettings { connectionPoolSettingsBuilder ->
                    ConnectionPoolSettings.builder()
                            .applyConnectionString(connectionString)
                            .maxWaitQueueSize(MAX_WAIT_QUEUE_SIZE).build()
                }
                .applyToServerSettings { serverSettingsBuilder ->
                    ServerSettings.builder()
                            .applyConnectionString(connectionString).build()
                }
                .credential(connectionString.credential!!)
                .streamFactoryFactory(NettyStreamFactoryFactory.builder().build())
                .applyToSocketSettings { socketSettingsBuilder ->
                    SocketSettings.builder()
                            .applyConnectionString(connectionString).build()
                }

        this.mongo = MongoClients.create(builder.build())
        return this.mongo
    }
}
