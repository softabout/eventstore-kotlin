package de.softabout

import org.springframework.beans.factory.BeanFactory
import org.springframework.beans.factory.NoSuchBeanDefinitionException
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.convert.CustomConversions
import org.springframework.data.mongodb.MongoDbFactory
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper
import org.springframework.data.mongodb.core.convert.MappingMongoConverter
import org.springframework.data.mongodb.core.mapping.MongoMappingContext

@Configuration
open class MongoSerializationConfig {

    @Bean
    open fun mappingMongoConverter(factory: MongoDbFactory, context: MongoMappingContext, beanFactory: BeanFactory): MappingMongoConverter {
        val dbRefResolver = DefaultDbRefResolver(factory)
        val mappingConverter = MappingMongoConverter(dbRefResolver, context)
        try {
            mappingConverter.setCustomConversions(beanFactory.getBean(CustomConversions::class.java))
        } catch (ignore: NoSuchBeanDefinitionException) {
            throw IllegalStateException("misconfiguration of mongoDb Access!")
        }

        // Don't save _class to mongo
        mappingConverter.typeMapper = DefaultMongoTypeMapper(null)

        return mappingConverter
    }

}
