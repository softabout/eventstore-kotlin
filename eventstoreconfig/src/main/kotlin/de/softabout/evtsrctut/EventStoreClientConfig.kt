package de.softabout.evtsrctut

import com.eventstore.dbclient.EventStoreDBClient
import com.eventstore.dbclient.EventStoreDBConnectionString
import com.eventstore.dbclient.EventStoreDBClientSettings
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.annotation.PreDestroy


@Configuration
open class EventStoreClientConfig() {

    private lateinit var eventStoreClient: EventStoreDBClient

    @PreDestroy
    fun close() {
        eventStoreClient.shutdown()
    }

    @Bean
    @ConditionalOnMissingBean
    open fun eventStoreClient(): EventStoreDBClient? {
        val settings: EventStoreDBClientSettings = EventStoreDBConnectionString.parse("esdb://localhost:2113?Tls=false")
        eventStoreClient = EventStoreDBClient.create(settings)
        return eventStoreClient
    }
}
