## Event Store Sample ##

Straight forward implementation of  "Real life event sourcing workshop Hotel room booking example" (implemented by Alexey Zimarev in .NET/C#) without sophisticated generic Aggregates and Store-Services.


- Start EventStoreDB and Mongo with docker-compose up -d
- Use your IDE or gradle to start Application in booking-domain (listens to port 8080 per default)
- Use tests in BookingControllerTest to create Bookings and Payments, adjust local variable bookingId 
- Start ProjectionService for ReadModel into Mongo with curl -X POST http://localhost:8080/projections/start

- Write useful domain code :-)