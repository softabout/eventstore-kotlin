package de.softabout.evtsrctut.domain.booking

import de.softabout.evtsrctut.domain.booking.infrastructure.store.BookingEventStore
import mu.KotlinLogging
import org.springframework.stereotype.Service

@Service
class BookingService(val bookingEventStore: BookingEventStore) {

    private val logger = KotlinLogging.logger {}
    fun applyCommandToExisting(cmd: Command) {
        val booking = Booking()
        bookingEventStore.loadEvents(cmd.bookingId)
                .forEach { booking.receiveEvent(it) }
        if(!booking.exists()) {
            logger.warn { "No Booking for ${cmd.bookingId} exists. Operation ${cmd.javaClass.simpleName} cannot be applied!" }
            return
        }
        val emittedEvents = when (cmd) {
            is RecordPayment -> booking.recordPayment(Money(cmd.amount, cmd.currency), cmd.paidBy, cmd.paidAt)
            is ApplyDiscount -> booking.applyDiscount(Money(cmd.amout, cmd.currency), cmd.appliedBy, cmd.appliedAt)
            else -> emptyList()
        }
        bookingEventStore.storeEvents(emittedEvents, cmd.bookingId)
    }

    fun applyCommandForNew(cmd: Command) {
        val booking = Booking()
        bookingEventStore.loadEvents(cmd.bookingId)
                .forEach { booking.receiveEvent(it) }
        if(booking.exists()) {
            logger.warn { "Booking for ${cmd.bookingId} already exists. Operation ${cmd.javaClass.simpleName} cannot be applied!" }
            return
        }
        val emittedEvents = when (cmd) {
            is Book -> booking.bookRoom(cmd.bookingId, cmd.guestId, cmd.roomId,
                    StayPeriod(cmd.from, cmd.to), Money(cmd.price, cmd.currency), cmd.bookedBy, cmd.bookedAt)
            else -> emptyList()
        }
        bookingEventStore.storeEvents(emittedEvents, cmd.bookingId)
    }
}
