package de.softabout.evtsrctut.domain.booking

import de.softabout.evtsrctut.domain.booking.infrastructure.BookingId
import de.softabout.evtsrctut.domain.booking.infrastructure.Currency
import de.softabout.evtsrctut.domain.booking.infrastructure.GuestId
import de.softabout.evtsrctut.domain.booking.infrastructure.RoomId
import java.util.Date

interface Command {
    val bookingId: BookingId
}

data class Book(
        override val bookingId: BookingId,
        val roomId: RoomId,
        val guestId: GuestId,
        val from: Date,
        val to: Date,
        val price: Double,
        val currency: Currency,
        val bookedBy: String,
        val bookedAt: Date
) : Command

data class RecordPayment(
        override val bookingId: BookingId,
        val amount: Double,
        val currency: Currency,
        val paidBy: String,
        val paidAt: Date
) : Command

data class ApplyDiscount(
        override val bookingId: BookingId,
        val amout: Double,
        val currency: Currency,
        val appliedBy: String,
        val appliedAt: Date
) : Command