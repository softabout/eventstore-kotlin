package de.softabout.evtsrctut.domain.booking.infrastructure.rest

import de.softabout.evtsrctut.domain.booking.ApplyDiscount
import de.softabout.evtsrctut.domain.booking.Book
import de.softabout.evtsrctut.domain.booking.BookingService
import de.softabout.evtsrctut.domain.booking.RecordPayment
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/bookings")
class BookingController(val bookingService: BookingService) {

    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getBookings(): ResponseEntity<Any> {
        return ResponseEntity.ok().build()
    }

    @PostMapping(path = ["/book"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun book(@RequestBody bookingCommand: Book) : ResponseEntity<Any> {
        bookingService.applyCommandForNew(bookingCommand)
        return ResponseEntity.ok().build()
    }

    @PostMapping(path = ["/pay"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun confirmPayment(@RequestBody recordPayment: RecordPayment) : ResponseEntity<Any> {
        bookingService.applyCommandToExisting(recordPayment)
        return ResponseEntity.ok().build()
    }

    @PostMapping(path = ["/discount"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun applyDiscount(@RequestBody applyDiscount: ApplyDiscount) : ResponseEntity<Any> {
        bookingService.applyCommandToExisting(applyDiscount)
        return ResponseEntity.ok().build()
    }
}