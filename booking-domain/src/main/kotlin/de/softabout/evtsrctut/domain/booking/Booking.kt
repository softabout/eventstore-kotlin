package de.softabout.evtsrctut.domain.booking

import de.softabout.evtsrctut.domain.booking.infrastructure.BookingId
import de.softabout.evtsrctut.domain.booking.infrastructure.GuestId
import de.softabout.evtsrctut.domain.booking.infrastructure.RoomId
import java.util.Date

class Booking {

    var bookingState: BookingState? = null

    fun exists() : Boolean = bookingState != null

    fun bookRoom(bookingId: BookingId, guestId: GuestId, roomId: RoomId, stayPeriod: StayPeriod, price: Money, bookedBy: String, bookedAt: Date): List<Event> {
        // put some logic in here :-)
        return listOf(RoomReserved(
                bookingId = bookingId,
                bookedAt = bookedAt,
                bookedBy = bookedBy,
                roomId = roomId,
                price = price.amount,
                currency = price.currency,
                guestId = guestId,
                checkinDate = stayPeriod.checkin,
                checkoutDate = stayPeriod.checkout
        ))
    }

    fun recordPayment(paid: Money, paidBy: String, paidAt: Date): List<Event> {
        // put some logic in here :-)
        return listOf(BookingPaid(
                bookingId = bookingState!!.bookingId,
                amountPaid = paid.amount,
                currency = paid.currency,
                fullyPaid = bookingState!!.outstanding.amount - paid.amount == 0.0,
                outstanding = bookingState!!.outstanding.amount - paid.amount,
                paidBy = paidBy,
                paidAt = paidAt
        ))
    }

    fun applyDiscount(discount: Money, appliedBy: String, appliedAt: Date): List<Event> {
        // put some logic in here :-)
        return listOf(DiscountApplied(
                bookingId = bookingState!!.bookingId,
                discount = discount.amount,
                currency = discount.currency,
                fullyPaid = bookingState!!.outstanding.amount - discount.amount == 0.0,
                outstanding = bookingState!!.outstanding.amount - discount.amount,
                appliedBy = appliedBy,
                appliedAt = appliedAt
        ))
    }

    fun receiveEvent(event: Event) {
        when (event) {
            is RoomReserved -> handleRoomReserved(event)
            is BookingPaid -> handleBookingPaid(event)
            is DiscountApplied -> handleDiscountApplied(event)
        }
    }

    private fun handleBookingPaid(event: BookingPaid) {
        bookingState = bookingState?.let { bookingState ->
            bookingState.copy(
                    payments = bookingState.payments + listOf(Payment(event.paidBy, Money(event.amountPaid, event.currency), paidAt = event.paidAt)),
                    outstanding = Money(bookingState.outstanding.amount - event.amountPaid, bookingState.outstanding.currency),
                    paid = bookingState.outstanding.amount - event.amountPaid == 0.0
            )
        }
    }

    private fun handleDiscountApplied(event: DiscountApplied) {
        bookingState = bookingState?.let { bookingState ->
            bookingState.copy(
                    payments = bookingState.payments + listOf(Payment(event.appliedBy, Money(event.discount, event.currency), paidAt = event.appliedAt)),
                    outstanding = Money(bookingState.outstanding.amount - event.discount, bookingState.outstanding.currency),
                    paid = bookingState.outstanding.amount - event.discount == 0.0
            )
        }
    }

    fun handleRoomReserved(event: RoomReserved) {
        bookingState = BookingState(
                bookingId = event.bookingId,
                guestId = event.guestId,
                roomId = event.roomId,
                period = StayPeriod(event.checkinDate, event.checkoutDate),
                price = Money(event.price, event.currency),
                outstanding = Money(event.price, event.currency),
                paid = false
        )
    }


    data class BookingState(
            val bookingId: BookingId,
            val guestId: GuestId,
            val roomId: RoomId,
            val period: StayPeriod,
            val price: Money,
            val outstanding: Money,
            val paid: Boolean,
            val payments: List<Payment> = emptyList()
    )

    data class Payment(
            val paidBy: String,
            val amount: Money,
            val paidAt: Date
    )
}