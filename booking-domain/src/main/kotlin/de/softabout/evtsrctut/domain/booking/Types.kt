package de.softabout.evtsrctut.domain.booking.infrastructure

import de.softabout.evtsrctut.domain.booking.BookingPaid
import de.softabout.evtsrctut.domain.booking.DiscountApplied
import de.softabout.evtsrctut.domain.booking.RoomReserved
import kotlin.reflect.KClass

typealias BookingId = String
typealias RoomId = String
typealias GuestId = String
typealias Currency = String

object EventClasses {
    val EVENT_CLASSES = mapOf<String, KClass<*>>(
            "RoomReserved" to RoomReserved::class,
            "BookingPaid" to BookingPaid::class,
            "DiscountApplied" to DiscountApplied::class
    )
}
