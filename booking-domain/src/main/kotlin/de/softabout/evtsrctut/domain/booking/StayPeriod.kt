package de.softabout.evtsrctut.domain.booking

import java.util.Date

data class StayPeriod
(
        val checkin: Date,
        val checkout: Date
)
