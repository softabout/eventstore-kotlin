package de.softabout.evtsrctut.domain.booking.infrastructure.store

import com.eventstore.dbclient.EventData
import com.eventstore.dbclient.EventStoreDBClient
import com.eventstore.dbclient.ReadStreamOptions
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import de.softabout.evtsrctut.domain.booking.Empty
import de.softabout.evtsrctut.domain.booking.Event
import de.softabout.evtsrctut.domain.booking.infrastructure.BookingId
import de.softabout.evtsrctut.domain.booking.infrastructure.EventClasses
import org.springframework.stereotype.Repository
import java.util.stream.Stream

@Repository
class BookingEventStore(val eventStoreDBClient: EventStoreDBClient) {

    val objectMapper: ObjectMapper = ObjectMapper().registerKotlinModule()

    fun storeEvents(events: List<Event>, bookingId: BookingId) {
        eventStoreDBClient.appendToStream("$BOOKING_STREAM-$bookingId",
                events.map { event ->
                    EventData.builderAsJson(event.javaClass.simpleName, event).build()
                }.iterator()
        )
    }

    fun loadEvents(bookingId: BookingId): Stream<Event> {
        try {
            return eventStoreDBClient.readStream("$BOOKING_STREAM-$bookingId", ReadStreamOptions.get().fromStart().forwards())
                    .get()
                    .events.stream()
                    .map { resolvedEvent ->
                        when (val eventClass = EventClasses.EVENT_CLASSES[resolvedEvent.event.eventType]) {
                            null -> Empty()
                            else -> objectMapper.readValue(resolvedEvent.event.eventData, eventClass.java)
                        } as Event
                    }
        } catch(e: Exception) {
            return Stream.empty()
        }
    }

    companion object {
        val BOOKING_STREAM = "Booking"
    }
}