package de.softabout.evtsrctut.domain.booking

import de.softabout.evtsrctut.domain.booking.infrastructure.Currency

data class Money(
        val amount: Double,
        val currency: Currency
)