package de.softabout.evtsrctut.domain.booking

import de.softabout.evtsrctut.domain.booking.infrastructure.BookingId
import de.softabout.evtsrctut.domain.booking.infrastructure.Currency
import de.softabout.evtsrctut.domain.booking.infrastructure.GuestId
import de.softabout.evtsrctut.domain.booking.infrastructure.RoomId
import java.util.Date

interface Event {
    val bookingId: BookingId;
}

data class Empty (
    override val bookingId: BookingId = ""
) : Event

data class RoomReserved(
        override val bookingId: BookingId,
        val roomId: RoomId,
        val guestId: GuestId,
        val checkinDate: Date,
        val checkoutDate: Date,
        val price: Double,
        val currency: Currency,
        val bookedBy: String,
        val bookedAt: Date
) : Event

data class BookingPaid(
        override val bookingId: BookingId,
        val amountPaid: Double,
        val currency: Currency,
        val fullyPaid: Boolean,
        val outstanding: Double,
        val paidBy: String,
        val paidAt: Date
) : Event

data class DiscountApplied(
        override val bookingId: BookingId,
        val discount: Double,
        val currency: Currency,
        val outstanding: Double,
        val fullyPaid: Boolean,
        val appliedBy: String,
        val appliedAt: Date
) : Event
