package de.softabout.evtsrctut

import mu.KotlinLogging
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableAsync
import java.util.*

private val log = KotlinLogging.logger {}

@EnableAsync
@SpringBootApplication
class Application {

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            Locale.setDefault(Locale.ENGLISH)
            SpringApplication.run(Application::class.java, *args)
        }
    }
}
