package de.softabout.evtsrctut.readmodel.booking

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "ReadModelPosition")
data class ReadModelPosition(
        @Id val collectionName: String,
        val commitUnsigned: Long,
        val prepareUnsigned: Long
)
