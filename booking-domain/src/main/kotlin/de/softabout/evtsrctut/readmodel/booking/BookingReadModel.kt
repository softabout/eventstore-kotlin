package de.softabout.evtsrctut.readmodel.booking

import de.softabout.evtsrctut.domain.booking.Booking
import de.softabout.evtsrctut.domain.booking.Money
import de.softabout.evtsrctut.domain.booking.StayPeriod
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

typealias BookingId = String

@Document(collection = "Booking")
data class BookingReadModel(
        @Id val bookingId: BookingId,
        val guestId: String,
        val roomId: String,
        val period: StayPeriod,
        val price: Money,
        val outstanding: Money,
        val paid: Boolean,
        val payments: List<Booking.Payment> = emptyList(),
        val version: Long
)



