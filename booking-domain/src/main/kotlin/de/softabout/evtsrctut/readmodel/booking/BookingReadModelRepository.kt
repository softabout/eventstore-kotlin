package de.softabout.evtsrctut.readmodel.booking

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface BookingReadModelRepository : MongoRepository<BookingReadModel, BookingId> {
    fun findByBookingId(bookingId: BookingId): BookingReadModel?
}
