package de.softabout.evtsrctut.readmodel.booking

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface ReadModelPositionRepository : MongoRepository<ReadModelPosition, String> {
    fun findByCollectionName(collectionName: String): ReadModelPosition?
}
