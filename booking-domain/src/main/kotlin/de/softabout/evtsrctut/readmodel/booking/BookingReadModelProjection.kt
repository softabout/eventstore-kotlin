package de.softabout.evtsrctut.readmodel.booking

import com.eventstore.dbclient.Position
import com.eventstore.dbclient.ResolvedEvent
import com.eventstore.dbclient.SubscribeToAllOptions
import com.eventstore.dbclient.Subscription
import com.eventstore.dbclient.SubscriptionFilter
import com.eventstore.dbclient.SubscriptionListener
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import de.softabout.evtsrctut.domain.booking.Booking
import de.softabout.evtsrctut.domain.booking.BookingPaid
import de.softabout.evtsrctut.domain.booking.DiscountApplied
import de.softabout.evtsrctut.domain.booking.Empty
import de.softabout.evtsrctut.domain.booking.Event
import de.softabout.evtsrctut.domain.booking.Money
import de.softabout.evtsrctut.domain.booking.RoomReserved
import de.softabout.evtsrctut.domain.booking.StayPeriod
import de.softabout.evtsrctut.domain.booking.infrastructure.EventClasses
import de.softabout.evtsrctut.domain.booking.infrastructure.store.BookingEventStore
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service

@Service
class BookingReadModelProjection(
        val bookingReadModelRepository: BookingReadModelRepository,
        val readModelPositionRepository: ReadModelPositionRepository,
        val eventStore: BookingEventStore
) {

    @Async
    fun startProjection() {
        val position = readModelPositionRepository.findByCollectionName("Booking")?.let {
            Position(it.commitUnsigned, it.prepareUnsigned)
        } ?: Position.START
        eventStore.eventStoreDBClient.subscribeToAll(BookingSubscriptionListener(bookingReadModelRepository, readModelPositionRepository),
                SubscribeToAllOptions.get()
                        .filter(SubscriptionFilter
                                .newBuilder()
                                .withStreamNamePrefix(BookingEventStore.BOOKING_STREAM)
                                .build())
                        .fromPosition(position)
        )

    }
}

class BookingSubscriptionListener(
        val bookingReadModelRepository: BookingReadModelRepository,
        val positionRepository: ReadModelPositionRepository
) : SubscriptionListener() {
    private val logger = KotlinLogging.logger {}
    val objectMapper: ObjectMapper = ObjectMapper().registerKotlinModule()

    override fun onEvent(subscription: Subscription?, event: ResolvedEvent?) {
        val position = event?.event?.position ?: Position.START
        positionRepository.save(ReadModelPosition("Booking", position.commitUnsigned, position.prepareUnsigned))
        val version = event?.event?.streamRevision?.valueUnsigned ?: 0L
        val bookingEvent = event?.let { resolvedEvent ->
            when (val eventClass = EventClasses.EVENT_CLASSES[resolvedEvent.event.eventType]) {
                null -> Empty()
                else -> objectMapper.readValue(resolvedEvent.event.eventData, eventClass.java)
            } as Event
        }
        when (bookingEvent) {
            is RoomReserved -> handleRoomReserved(bookingEvent, version)
            is BookingPaid -> handleBookingPaid(bookingEvent, version)
            is DiscountApplied -> handleDiscountApplied(bookingEvent, version)
            else -> doNothing()
        }
    }

    fun handleRoomReserved(roomReserved: RoomReserved, version: Long) {
        val bookingReadModel = BookingReadModel(
                bookingId = roomReserved.bookingId,
                guestId = roomReserved.guestId,
                roomId = roomReserved.roomId,
                period = StayPeriod(roomReserved.checkinDate, roomReserved.checkoutDate),
                price = Money(roomReserved.price, roomReserved.currency),
                outstanding = Money(roomReserved.price, roomReserved.currency),
                paid = false,
                version = version)
        bookingReadModelRepository.save(bookingReadModel)
    }

    fun handleBookingPaid(bookingPaid: BookingPaid, version: Long) {
        bookingReadModelRepository.findByBookingId(bookingPaid.bookingId)?.let { bookingReadModel ->
            if (version > bookingReadModel.version) {
                val updatedReadModel = bookingReadModel.copy(
                        payments = bookingReadModel.payments + listOf(Booking.Payment(bookingPaid.paidBy, Money(bookingPaid.amountPaid, bookingPaid.currency), paidAt = bookingPaid.paidAt)),
                        outstanding = Money(bookingReadModel.outstanding.amount - bookingPaid.amountPaid, bookingReadModel.outstanding.currency),
                        paid = bookingReadModel.outstanding.amount - bookingPaid.amountPaid == 0.0,
                        version = version
                )
                bookingReadModelRepository.save(updatedReadModel)
            } else {
                logger.warn { "Version $version of ${bookingPaid.javaClass.simpleName} event already applied!" }
            }
        }
    }

    fun handleDiscountApplied(discountApplied: DiscountApplied, version: Long) {
        bookingReadModelRepository.findByBookingId(discountApplied.bookingId)?.let { bookingReadModel ->
            if (version > bookingReadModel.version) {
                val updatedReadModel = bookingReadModel.copy(
                        payments = bookingReadModel.payments + listOf(Booking.Payment(discountApplied.appliedBy, Money(discountApplied.discount, discountApplied.currency), paidAt = discountApplied.appliedAt)),
                        outstanding = Money(bookingReadModel.outstanding.amount - discountApplied.discount, bookingReadModel.outstanding.currency),
                        paid = bookingReadModel.outstanding.amount - discountApplied.discount == 0.0,
                        version = version
                )
                bookingReadModelRepository.save(updatedReadModel)
            } else {
                logger.warn { "Version $version of ${discountApplied.javaClass.simpleName} event already applied!" }
            }
        }
    }

    fun doNothing() {}
}