package de.softabout.evtsrctut.readmodel.booking.infrastructure.rest

import de.softabout.evtsrctut.readmodel.booking.BookingReadModelProjection
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/projections")
class ReadModelController(
        val bookingReadModelProjection: BookingReadModelProjection
) {

    @PostMapping(path = ["/start"])
    fun start() {
        bookingReadModelProjection.startProjection()
    }
}