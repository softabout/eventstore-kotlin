package de.softabout.evtsrctut.domain.booking.infrastructure.rest

import com.jayway.restassured.RestAssured
import com.jayway.restassured.http.ContentType
import com.jayway.restassured.specification.RequestSpecification
import de.softabout.evtsrctut.domain.booking.ApplyDiscount
import de.softabout.evtsrctut.domain.booking.Book
import de.softabout.evtsrctut.domain.booking.RecordPayment
import org.apache.http.HttpStatus
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import java.util.*

internal class BookingControllerTest {

    val bookingId = "4"

    @Order(1)
    @Test
    fun book() {
        val book = Book(
                        bookingId = bookingId,
                        roomId = "1",
                        guestId = "1",
                        from = Date(),
                        to = Date(),
                        price = 120.0,
                        currency = "EUR",
                        bookedAt = Date(),
                        bookedBy = "memyselfandi"
                )
        givenLocal()
                .contentType(ContentType.JSON)
                .body(book)
                .post("book")
    }

    @Order(2)
    @Test
    fun pay() {
        givenLocal()
                .expect().statusCode(HttpStatus.SC_OK)
                .with()
                .contentType(ContentType.JSON)
                .body(RecordPayment(
                        bookingId = bookingId,
                        amount = 25.0,
                        currency = "EUR",
                        paidAt = Date(),
                        paidBy = "memyselfandi"
                ))
                .post("/pay")
    }
    @Order(3)
    @Test
    fun discount() {
        givenLocal()
                .expect().statusCode(HttpStatus.SC_OK)
                .with()
                .contentType(ContentType.JSON)
                .body(ApplyDiscount(
                        bookingId = bookingId,
                        amout = 20.0,
                        currency = "EUR",
                        appliedAt = Date(),
                        appliedBy = "friendlyservice"
                ))
                .post("/discount")
    }
}


fun givenLocal() : RequestSpecification {
    return RestAssured.given().log().all().baseUri("http://localhost:8080/bookings")
}